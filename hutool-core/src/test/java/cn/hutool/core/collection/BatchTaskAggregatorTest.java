package cn.hutool.core.collection;

import cn.hutool.core.lang.Console;
import org.junit.jupiter.api.Test;

/**
 * <p></p>
 * <p>2025-01-17 13:16</p>
 *
 * @author Dan
 **/
public class BatchTaskAggregatorTest {

	@Test
	void testSubmitAndProcessTasks() {
		try (BatchTaskAggregator<String> queueProcessor = new BatchTaskAggregator<>(10, data -> {
			// 异步任务消费
			Console.log("批量处理：{} 条，数据：{}", data.size(), data);
		})) {
			for (int i = 1; i <= 100; i++) {
				// 任务提交队列
				queueProcessor.add(String.valueOf(i));
			}

			// 等待队列消费完成
			queueProcessor.waitCompletion();
			Console.log("队列消费完成");
		} catch (Exception e) {
			Console.log(e);
		}
	}
}
