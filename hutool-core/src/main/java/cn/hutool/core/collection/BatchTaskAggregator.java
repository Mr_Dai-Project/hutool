package cn.hutool.core.collection;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;

/**
 * <p>批量任务处理工具</p>
 * <p>主要目的是为了将单条数据进行整合后，在固定时间内将数据合并成一个集合，进行批量消费处理</p>
 * <p>且限制每次处理的最大数据量（即 maxTask）</p>
 * <p>针对于无法进行批量查询时的场景，这种方式能够提高吞吐量、减少每次处理时的开销</p>
 * <p>2025-01-17 12:44</p>
 *
 * @author Dan
 **/
public class BatchTaskAggregator<T> implements Closeable {

	/**
	 * 队列
	 */
	private final BlockingQueue<T> queue;

	/**
	 * 最大任务数
	 */
	private final int maxTask;

	/**
	 * 持续时间
	 */
	private final long duration;

	/**
	 * 超时时间单位
	 */
	private final TimeUnit unit;

	/**
	 * 自定义的消费者逻辑处理
	 */
	private final Consumer<List<T>> consumer;

	/**
	 * 线程池用于异步消费数据
	 */
	private final ScheduledExecutorService executorService;

	/**
	 * 存取器
	 */
	private final CountDownLatch completionLatch = new CountDownLatch(1);

	/**
	 * 初始化
	 *
	 * @param maxTask  单次消费时的最大任务数
	 * @param consumer 自定义的消费者逻辑处理
	 */
	public BatchTaskAggregator(int maxTask, Consumer<List<T>> consumer) {
		// 默认持续1s，使用无界队列
		this(1L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), maxTask, consumer);
	}

	/**
	 * 初始化
	 *
	 * @param duration 持续时间
	 * @param unit     超时时间单位
	 * @param queue    队列
	 * @param maxTask  单次消费时的最大任务数
	 * @param consumer 自定义的消费者逻辑处理
	 */
	public BatchTaskAggregator(long duration, TimeUnit unit, BlockingQueue<T> queue, int maxTask, Consumer<List<T>> consumer) {
		this.queue = queue;
		this.maxTask = maxTask;
		this.consumer = consumer;
		this.duration = duration;
		this.unit = unit;

		// 初始化线程池
		this.executorService = new ScheduledThreadPoolExecutor(1, runnable -> {
			Thread thread = new Thread(runnable, "queue-batch-thread");
			thread.setDaemon(true);
			return thread;
		});

		// 开启线程
		this.executorService.scheduleAtFixedRate(this::handlerWorker, duration, duration, unit);
		Runtime.getRuntime().addShutdownHook(new Thread(this::destroy));
	}

	/**
	 * 从队列中获取指定数量的数据，可用于批量处理
	 * 每次获取指定数量的数据，如果队列中数据量不足，则等待一段时间后获取所有数据
	 *
	 * @param queue     队列
	 * @param batchSize 批量大小
	 * @param duration  持续时间
	 * @param unit      超时时间单位
	 * @param <T>       队列元素类型
	 * @return 队列元素集合
	 * @throws InterruptedException 如果在等待时被打断
	 */
	private static <T> List<T> pollBatchOrWait(BlockingQueue<T> queue, int batchSize, long duration, TimeUnit unit) throws InterruptedException {
		// 创建一个具有指定初始容量的空ArrayList实例，用于保存队列中的元素
		List<T> buffer = new ArrayList<>(batchSize);

		// 持续时间
		long deadline = System.nanoTime() + unit.toNanos(duration);
		int added = 0;

		while (added < batchSize) {
			// 从队列中删除指定数量的可用元素，并将它们添加到指定集合中
			added += queue.drainTo(buffer, batchSize - added);
			if (added < batchSize) {
				// 取出来队列第一个元素并删除，可等待指定的等待时间以使元素变为可用，如果队列为空，则返回null
				T element = queue.poll(deadline - System.nanoTime(), java.util.concurrent.TimeUnit.NANOSECONDS);
				if (element == null) {
					break;
				}
				// 添加到集合中
				buffer.add(element);
				++added;
			}
		}
		return buffer;
	}

	/**
	 * 将指定元素插入此队列，必要时等待空间可用。
	 *
	 * @param element 要添加的元素
	 * @throws InterruptedException 如果在等待时被打断，抛出异常
	 */
	public void add(T element) throws InterruptedException {
		queue.put(element);
	}

	/**
	 * 阻塞当前线程，等待队列消费完成
	 *
	 * @throws InterruptedException 如果在等待时被打断，抛出异常
	 */
	public void waitCompletion() throws InterruptedException {
		completionLatch.await();
	}

	/**
	 * 异步，等待处理
	 */
	private void handlerWorker() {
		try {
			// 从队列中获取指定条目的数据，不足时等待1秒，汇总上报消费一次
			List<T> arrayList = pollBatchOrWait(this.queue, this.maxTask, this.duration, this.unit);
			if (CollectionUtil.isEmpty(arrayList)) {
				// 队列空了，唤醒线程
				completionLatch.countDown();
				return;
			}

			// 集中消费一次
			this.consumer.accept(arrayList);
		} catch (InterruptedException e) {
			// 重新设置中断标志位
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * executor服务的销毁
	 */
	private void destroy() {
		if (executorService != null) {
			executorService.shutdownNow();
			while (true) {
				try {
					// 等待500ms看线程池是否成功关闭，如果关闭则跳出循环
					long timeout = 500;
					if (executorService.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
						break;
					}
				} catch (InterruptedException e) {
					// Restore interrupted status
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	@Override
	public void close() throws IOException {
		this.destroy();
	}
}
